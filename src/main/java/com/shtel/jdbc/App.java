package com.shtel.jdbc;

import com.shtel.jdbc.entity.User;
import com.shtel.jdbc.service.UserService;
import com.shtel.jdbc.service.UserServiceImpl;

import java.util.List;

/**
 * @author 王坤
 * @version 1.0.0
 * @Description
 * @date 2018/9/3
 * 版权所有 (c) 2018
 */
public class App {
    public static void main(String[] args) {
        UserService userService = new UserServiceImpl();
        System.out.println("连接成功");

        //插入学生
        userService.insertBatch();

        List<User> all = userService.findAll();
        for (User user : all) {
            System.out.println(user);
        }

        //根据学号查询
        List<User> all2 = userService.findStuBySno("4");
        for (User user : all2) {
            System.out.println(user);
        }

        //根据学号删除
        userService.deleteStuBySno("110");
        //根据学号修改年龄
        userService.updateStuBySno(20,"1");//根据条件修改学生


      /*  userService.insertBatch();*/
    }
}
